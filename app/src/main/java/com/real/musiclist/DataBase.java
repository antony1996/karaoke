package com.real.musiclist;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.real.musiclist.Model.Disk;
import com.real.musiclist.Model.Song;

import java.util.ArrayList;

public class DataBase {

    private final String DATA_BASE_NAME = "MusicData.db";
    private final String DISK_TABLE = "Disk";
    private final String SONG_TABLE = "Song";

    Context context;

    DataBase (Context context){
        this.context = context;
    }

    void openOfCreateDatabase() {
        SQLiteDatabase database = context.openOrCreateDatabase(DATA_BASE_NAME, Context.MODE_PRIVATE, null);
        database.execSQL("CREATE TABLE IF NOT EXISTS " + DISK_TABLE +" (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "number INTEGER," +
                "name STRING" +
                ");");
        database.execSQL("CREATE TABLE IF NOT EXISTS " + SONG_TABLE +" (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "id INTEGER," +
                "singer STRING," +
                "song_name STRING," +
                "disk_number INTEGER" +
                ");");
        database.close();
    }

    void insertDisk (int number, String name) {
        SQLiteDatabase database = context.openOrCreateDatabase(DATA_BASE_NAME, Context.MODE_PRIVATE, null);
        ContentValues values = new ContentValues();
        values.put("number", number);
        values.put("name", name);
        database.insert(DISK_TABLE, null, values);
        database.close();
    }

    void insertSong (int id, String singer, String songName, int disk_number) {
        SQLiteDatabase database = context.openOrCreateDatabase(DATA_BASE_NAME, Context.MODE_PRIVATE, null);
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("singer", singer);
        values.put("song_name", songName);
        values.put("disk_number", disk_number);
        database.insert(SONG_TABLE, null, values);
        database.close();
    }

    ArrayList<Disk> getAllDiskList() {
        ArrayList<Disk> list = new ArrayList<>();
        SQLiteDatabase database = context.openOrCreateDatabase(DATA_BASE_NAME, Context.MODE_PRIVATE, null);
        Cursor cursor = database.query(DISK_TABLE, null, null, null, null, null, "number");
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Disk disk = new Disk(cursor.getInt(1), cursor.getString(2));
            list.add(disk);
            cursor.moveToNext();
        }
        cursor.close();
        database.close();
        return list;
    }

    ArrayList<Song> getAllSongList(String diskID) {
        ArrayList<Song> list = new ArrayList<>();
        String [] selectionArgs = {diskID};
        SQLiteDatabase database = context.openOrCreateDatabase(DATA_BASE_NAME, Context.MODE_PRIVATE, null);
        Cursor cursor = database.query(SONG_TABLE, null, "disk_number = ?", selectionArgs, null, null, "singer");
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Song song = new Song(cursor.getInt(1), cursor.getString(2), cursor.getString(3));
            list.add(song);
            cursor.moveToNext();
        }
        cursor.close();
        database.close();
        return list;
    }

    ArrayList<Integer> getAllDiskID() {
        ArrayList<Integer> list = new ArrayList<>();
        SQLiteDatabase database = context.openOrCreateDatabase(DATA_BASE_NAME, Context.MODE_PRIVATE, null);
        Cursor cursor = database.query(DISK_TABLE, new String[]{"number"}, null, null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            int disk = cursor.getInt(0);
            list.add(disk);
            cursor.moveToNext();
        }
        cursor.close();
        database.close();
        return list;
    }

    ArrayList<Integer> getAllSongID(String disk_number) {
        ArrayList<Integer> list = new ArrayList<>();
        SQLiteDatabase database = context.openOrCreateDatabase(DATA_BASE_NAME, Context.MODE_PRIVATE, null);
        Cursor cursor = database.query(SONG_TABLE, new String[]{"id"}, "disk_number = ?", new String[]{disk_number}, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            int song = cursor.getInt(0);
            list.add(song);
            cursor.moveToNext();
        }
        cursor.close();
        database.close();
        return list;
    }



    void deleteDisk(String number) {
        SQLiteDatabase database = context.openOrCreateDatabase(DATA_BASE_NAME, Context.MODE_PRIVATE, null);
        database.delete(DISK_TABLE, "number = ?", new String[]{number});
        database.delete(SONG_TABLE, "disk_number = ?", new String[]{number});
        database.close();
    }

    void deleteSong(String diskNumber, String id) {
        SQLiteDatabase database = context.openOrCreateDatabase(DATA_BASE_NAME, Context.MODE_PRIVATE, null);
        database.delete(SONG_TABLE, "disk_number = ? AND id = ?", new String[]{diskNumber, id});
        database.close();
    }

    void updateDisk(String number, String name, String id){
        SQLiteDatabase database = context.openOrCreateDatabase(DATA_BASE_NAME, Context.MODE_PRIVATE, null);
        ContentValues updateNumber = new ContentValues();
        ContentValues updateName = new ContentValues();
        updateNumber.put("number", number);
        updateName.put("name", name);
        database.update(DISK_TABLE, updateName, "number = ?", new String[]{id});
        database.update(DISK_TABLE, updateNumber, "number = ?", new String[]{id});

        ContentValues updateSongDiskNumber = new ContentValues();
        updateSongDiskNumber.put("disk_number", number);
        database.update(SONG_TABLE, updateSongDiskNumber, "disk_number = ?", new String[]{id});

        database.close();
    }
    void updateSong(String id, String singer, String songName, String oldID, String diskID){
        SQLiteDatabase database = context.openOrCreateDatabase(DATA_BASE_NAME, Context.MODE_PRIVATE, null);
        ContentValues updateId = new ContentValues();
        ContentValues updateSinger = new ContentValues();
        ContentValues updateSong = new ContentValues();
        updateId.put("id", id);
        updateSinger.put("singer", singer);
        updateSong.put("song_name", songName);
        database.update(SONG_TABLE, updateId, "id = ? AND disk_number = ?", new String[]{oldID, diskID});
        database.update(SONG_TABLE, updateSinger, "id = ? AND disk_number = ?", new String[]{oldID, diskID});
        database.update(SONG_TABLE, updateSong, "id = ? AND disk_number = ?", new String[]{oldID, diskID});
        database.close();
    }


}
