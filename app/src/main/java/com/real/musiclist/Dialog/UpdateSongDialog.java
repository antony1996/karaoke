package com.real.musiclist.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.real.musiclist.R;
import com.real.musiclist.SongActivity;


public class UpdateSongDialog extends DialogFragment {

    int position;
    String oldID;
    String diskID;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle bundle = getArguments();
        String songID = bundle.getString("oldID");
        String singer = bundle.getString("singer");
        String songName = bundle.getString("songName");

        oldID = bundle.getString("oldID");
        diskID = bundle.getString("diskID");
        position = bundle.getInt("position");

        final float dimen = getActivity().getResources().getDimension(R.dimen.text_size);
        final float dimenButtons = getActivity().getResources().getDimension(R.dimen.text_size_for_dialog);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_update_song, null);
        final EditText etIDSong = (EditText) view.findViewById(R.id.etIDSong);
        final EditText etSinger = (EditText) view.findViewById(R.id.etSinger);
        final EditText etSongName = (EditText) view.findViewById(R.id.etSongName);
        etIDSong.setText(songID);
        etSinger.setText(singer);
        etSongName.setText(songName);
        TextView textView = new TextView(getActivity());
        textView.setText("Редактирование песни");
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(dimen);
        builder.setCustomTitle(textView);
        builder.setPositiveButton("Подтвердить", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(!etIDSong.getText().toString().isEmpty() && !etSinger.getText().toString().isEmpty() && !etSongName.getText().toString().isEmpty()) {
                    ((SongActivity)getActivity()).updateSong(etIDSong.getText().toString(), etSinger.getText().toString(), etSongName.getText().toString(), oldID, diskID, position);
                }else{
                    Toast.makeText(getActivity(), "Заполните все поля", Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton("Отменить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.setView(view);

        final AlertDialog alert = builder.create();
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button btnPositive = alert.getButton(Dialog.BUTTON_POSITIVE);
                btnPositive.setTextSize(dimenButtons);

                Button btnNegative = alert.getButton(Dialog.BUTTON_NEGATIVE);
                btnNegative.setTextSize(dimenButtons);
            }
        });

        return alert;
    }

}
