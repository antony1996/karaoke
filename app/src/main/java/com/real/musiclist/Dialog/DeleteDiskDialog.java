package com.real.musiclist.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.widget.Button;
import android.widget.TextView;

import com.real.musiclist.DiskActivity;
import com.real.musiclist.R;


public class DeleteDiskDialog extends DialogFragment {

    String diskID;
    String diskName;
    int position;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle bundle = getArguments();
        diskID = bundle.getString("diskID");
        diskName = bundle.getString("diskName");
        position = bundle.getInt("position");

        final float dimen = getActivity().getResources().getDimension(R.dimen.text_size);
        final float dimenButtons = getActivity().getResources().getDimension(R.dimen.text_size_for_dialog);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        TextView textView = new TextView(getActivity());
        textView.setText("Удалить диск №" + diskID + " " + diskName);
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(dimen);
        builder.setCustomTitle(textView);
        builder.setPositiveButton("Удалить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                    ((DiskActivity)getActivity()).deleteDisk(diskID, position);
            }
        });
        builder.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });

        final AlertDialog alert = builder.create();
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button btnPositive = alert.getButton(Dialog.BUTTON_POSITIVE);
                btnPositive.setTextSize(dimenButtons);

                Button btnNegative = alert.getButton(Dialog.BUTTON_NEGATIVE);
                btnNegative.setTextSize(dimenButtons);
            }
        });

        return alert;
    }
}
