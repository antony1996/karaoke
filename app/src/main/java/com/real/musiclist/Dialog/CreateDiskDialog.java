package com.real.musiclist.Dialog;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.real.musiclist.DiskActivity;
import com.real.musiclist.R;

public class CreateDiskDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final float dimen = getActivity().getResources().getDimension(R.dimen.text_size);
        final float dimenButtons = getActivity().getResources().getDimension(R.dimen.text_size_for_dialog);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_create_disk, null);
        final EditText etNumber = (EditText) view.findViewById(R.id.etNumber);
        final EditText etDiskName = (EditText) view.findViewById(R.id.etDiskName);
        etDiskName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        TextView textView = new TextView(getActivity());
        textView.setText("Создание диска");
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(dimen);
        builder.setCustomTitle(textView);
        builder.setPositiveButton("Создать", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(!etDiskName.getText().toString().isEmpty() && !etNumber.getText().toString().isEmpty()) {
                    ((DiskActivity) getActivity()).addDisk(Integer.parseInt(etNumber.getText().toString()), etDiskName.getText().toString());
                }else{
                    Toast.makeText(getActivity(), "Заполните все поля", Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.setView(view);

        final AlertDialog alert = builder.create();
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button btnPositive = alert.getButton(Dialog.BUTTON_POSITIVE);
                btnPositive.setTextSize(dimenButtons);

                Button btnNegative = alert.getButton(Dialog.BUTTON_NEGATIVE);
                btnNegative.setTextSize(dimenButtons);
            }
        });

        alert.getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        return alert;
    }
}
