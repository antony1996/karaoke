package com.real.musiclist.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.widget.Button;
import android.widget.TextView;

import com.real.musiclist.R;
import com.real.musiclist.SongActivity;


public class DeleteSongDialog extends DialogFragment {

    String id;
    String songName;
    int position;
    String diskID;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Bundle bundle = getArguments();
        id = bundle.getString("id");
        songName = bundle.getString("songName");
        position = bundle.getInt("position");
        diskID = bundle.getString("diskID");

        final float dimen = getActivity().getResources().getDimension(R.dimen.text_size);
        final float dimenButtons = getActivity().getResources().getDimension(R.dimen.text_size_for_dialog);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        TextView textView = new TextView(getActivity());
        textView.setText("Удалить песню: " + songName);
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(dimen);
        builder.setCustomTitle(textView);
        builder.setPositiveButton("Удалить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ((SongActivity)getActivity()).deleteSong(diskID, id, position);
            }
        });
        builder.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });

        final AlertDialog alert = builder.create();
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button btnPositive = alert.getButton(Dialog.BUTTON_POSITIVE);
                btnPositive.setTextSize(dimenButtons);

                Button btnNegative = alert.getButton(Dialog.BUTTON_NEGATIVE);
                btnNegative.setTextSize(dimenButtons);
            }
        });

        return alert;
    }
}
