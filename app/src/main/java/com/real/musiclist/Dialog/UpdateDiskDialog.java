package com.real.musiclist.Dialog;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.real.musiclist.DiskActivity;
import com.real.musiclist.R;

public class UpdateDiskDialog extends DialogFragment {

    String diskID;
    String diskName;
    int position;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final float dimen = getActivity().getResources().getDimension(R.dimen.text_size);
        final float dimenButtons = getActivity().getResources().getDimension(R.dimen.text_size_for_dialog);

        Bundle bundle = getArguments();
        diskID = bundle.getString("diskID");
        diskName = bundle.getString("diskName");
        position = bundle.getInt("position");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_update_disk, null);
        final EditText etNumber = (EditText) view.findViewById(R.id.etNumber);
        final EditText etDiskName = (EditText) view.findViewById(R.id.etDiskName);
        etNumber.setText(diskID);
        etDiskName.setText(diskName);
        TextView textView = new TextView(getActivity());
        textView.setText("Редактирование диска");
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(dimen);
        builder.setCustomTitle(textView);
        builder.setPositiveButton("Подтвердить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(!etDiskName.getText().toString().isEmpty() && !etNumber.getText().toString().isEmpty()) {
                    ((DiskActivity)getActivity()).updateDisk(etNumber.getText().toString(), etDiskName.getText().toString(), position, diskID);
                }else{
                    Toast.makeText(getActivity(), "Заполните все поля", Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton("Отменить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.setView(view);

        final AlertDialog alert = builder.create();
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button btnPositive = alert.getButton(Dialog.BUTTON_POSITIVE);
                btnPositive.setTextSize(dimenButtons);

                Button btnNegative = alert.getButton(Dialog.BUTTON_NEGATIVE);
                btnNegative.setTextSize(dimenButtons);
            }
        });

        return alert;
    }
}
