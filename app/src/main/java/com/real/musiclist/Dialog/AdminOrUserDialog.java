package com.real.musiclist.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.real.musiclist.DiskActivity;
import com.real.musiclist.R;

import static com.real.musiclist.DiskActivity.admin;


public class AdminOrUserDialog extends DialogFragment {
    String pass = "0000";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_admin_or_user, null);
        final EditText etNumber = (EditText) view.findViewById(R.id.etPass);
        builder.setTitle("Войти!");
        builder.setCancelable(false);
        Button bAdmin = (Button) view.findViewById(R.id.bAdmin);
        bAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etNumber.length() != 0) {
                    if (etNumber.getText().toString().equals(pass)) {
                        admin = true;
                        ((DiskActivity) getActivity()).fab.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), "Вы вошли как администратор", Toast.LENGTH_LONG).show();
                    } else
                        admin = false;
                } else {
                    admin = false;
                }
                dismiss();
            }
        });

        builder.setView(view);

        final AlertDialog alert = builder.create();
        alert.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        return alert;

    }
}
