package com.real.musiclist;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.real.musiclist.Adapter.DisksAdapter;
import com.real.musiclist.Dialog.AdminOrUserDialog;
import com.real.musiclist.Dialog.CreateDiskDialog;
import com.real.musiclist.Dialog.DeleteDiskDialog;
import com.real.musiclist.Model.Disk;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.util.ArrayList;

public class DiskActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    ListView listOfDisks;
    ArrayAdapter arrayAdapter;
    ArrayList<Disk> disks;

    DataBase dataBase;
    public FloatingActionButton fab;

    public static boolean admin = false;
    private static boolean firstCome = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disk);
        dataBase =  new DataBase(getApplicationContext());
        dataBase.openOfCreateDatabase();

        SharedPreferences sp = getSharedPreferences("isFirst", Context.MODE_PRIVATE);
        boolean hasVisited = sp.getBoolean("hasVisited", false);
        if (!hasVisited) {
            createTestDiskAndImportSongs();
            SharedPreferences.Editor e = sp.edit();
            e.putBoolean("hasVisited", true);
            e.apply();
        }

        disks = dataBase.getAllDiskList();

        if(!firstCome) {
            AdminOrUserDialog adminOrUserDialog = new AdminOrUserDialog();
            adminOrUserDialog.setCancelable(false);
            adminOrUserDialog.show(getFragmentManager(), "adminOrUser");
        }
        firstCome = true;

        listOfDisks = (ListView) findViewById(R.id.listOfDisks);
        arrayAdapter = new DisksAdapter(this, R.layout.item_of_disk, disks);
        listOfDisks.setAdapter(arrayAdapter);
        listOfDisks.setOnItemClickListener(this);
        listOfDisks.setOnItemLongClickListener(this);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CreateDiskDialog c = new CreateDiskDialog();
                c.show(getFragmentManager(), "dialog");
            }
        });
        fab.setVisibility(View.GONE);

        setTitle("Караоке ГК Медвежий угол");
    }

    private void createTestDiskAndImportSongs(){
        dataBase.insertDisk(1, "Диск 1");
        dataBase.insertDisk(2, "Диск 2");
        dataBase.insertDisk(3, "Диск 3");
        dataBase.insertDisk(4, "Диск 4");
        dataBase.insertDisk(5, "Диск 5");
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Workbook wb = WorkbookFactory.create(getAssets().open("songs.xls"));
                    Sheet sheet = wb.getSheetAt(0);
                    for(int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
                        Row row = sheet.getRow(i);
                        Cell id = row.getCell(0);
                        Cell song = row.getCell(1);
                        Cell singer = row.getCell(2);
                        int songID = (int) id.getNumericCellValue();
                        String songName = song.getStringCellValue();
                        String songSinger = singer.getStringCellValue();
                        dataBase.insertSong(songID, songSinger, songName, 1);
                    }
                } catch(Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Workbook wb = WorkbookFactory.create(getAssets().open("karaoke.xls"));
                    Sheet sheet = wb.getSheetAt(0);
                    int songID;
                    String songName;
                    String songSinger;
                    for(int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
                        Row row = sheet.getRow(i);

                        Cell id = row.getCell(0);
                        Cell song = row.getCell(1);
                        Cell singer = row.getCell(2);

                        songID = (int) id.getNumericCellValue();
                        songName = song.getStringCellValue();
                        songSinger = singer.getStringCellValue();

                        dataBase.insertSong(songID, songSinger, songName, 2);

                        Cell id1 = row.getCell(4);
                        Cell song1 = row.getCell(5);
                        Cell singer1 = row.getCell(6);

                        songID = (int) id1.getNumericCellValue();
                        songName = song1.getStringCellValue();
                        songSinger = singer1.getStringCellValue();

                        dataBase.insertSong(songID, songSinger, songName, 3);

                        Cell id2 = row.getCell(8);
                        Cell song2 = row.getCell(9);
                        Cell singer2 = row.getCell(10);

                        songID = (int) id2.getNumericCellValue();
                        songName = song2.getStringCellValue();
                        songSinger = singer2.getStringCellValue();

                        dataBase.insertSong(songID, songSinger, songName, 4);

                        Cell id3 = row.getCell(12);
                        Cell song3 = row.getCell(13);
                        Cell singer3 = row.getCell(14);

                        songID = (int) id3.getNumericCellValue();
                        songName = song3.getStringCellValue();
                        songSinger = singer3.getStringCellValue();

                        dataBase.insertSong(songID, songSinger, songName, 5);

                    }
                } catch(Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }).start();

    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(getApplicationContext(), SongActivity.class);
        TextView num = (TextView) view.findViewById(R.id.tvNumberOfDisks);
        String diskID = num.getText().toString();
        TextView name = (TextView) view.findViewById(R.id.tvDiskName);
        String diskName = name.getText().toString();
        intent.putExtra("diskID", diskID);
        intent.putExtra("diskName", diskName);
        startActivity(intent);
    }

    public void addDisk(int number, String name) {
        if (!dataBase.getAllDiskID().contains(number)) {
            dataBase.insertDisk(number, name);
            disks.add(new Disk(number, name));
            arrayAdapter.notifyDataSetChanged();
        }else{
            Toast.makeText(this, "Диск с таким № уже существует", Toast.LENGTH_LONG).show();
        }
    }

    public void deleteDisk(String diskID, int position){
                dataBase.deleteDisk(diskID);
                disks.remove(position);
                arrayAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        if(admin) {
            DeleteDiskDialog deleteDiskDialog = new DeleteDiskDialog();
            TextView num = (TextView) view.findViewById(R.id.tvNumberOfDisks);
            TextView text = (TextView) view.findViewById(R.id.tvDiskName);
            String diskID = num.getText().toString();
            String diskName = text.getText().toString();
            Bundle bundle = new Bundle();
            bundle.putString("diskID", diskID);
            bundle.putString("diskName", diskName);
            bundle.putInt("position", i);
            deleteDiskDialog.setArguments(bundle);
            deleteDiskDialog.show(getSupportFragmentManager(), "deleteDiskDialog");
        }

//        UpdateDiskDialog updateDiskDialog = new UpdateDiskDialog();
//        TextView num = (TextView) view.findViewById(R.id.tvNumberOfDisks);
//        TextView text = (TextView) view.findViewById(R.id.tvDiskName);
//        String diskID = num.getText().toString();
//        String diskName = text.getText().toString();
//        Bundle bundle = new Bundle();
//        bundle.putString("diskID", diskID);
//        bundle.putString("diskName", diskName);
//        bundle.putInt("position", i);
//        updateDiskDialog.setArguments(bundle);
//        updateDiskDialog.show(getSupportFragmentManager(), "updateDiskDialog");

        return true;
    }

    public void updateDisk(String number, String name, int position, String diskID){
        if (!dataBase.getAllDiskID().contains(Integer.parseInt(number))) {
        dataBase.updateDisk(number, name, diskID);
        disks.get(position).setName(name);
        disks.get(position).setNubmer(Integer.parseInt(number));
        arrayAdapter.notifyDataSetChanged();
        }else{
            Toast.makeText(this, "Диск с таким № уже существует", Toast.LENGTH_LONG).show();
        }
    }
}
