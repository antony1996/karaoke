package com.real.musiclist;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.real.musiclist.Adapter.SongAdapter;
import com.real.musiclist.Dialog.CreateSongDialog;
import com.real.musiclist.Dialog.DeleteSongDialog;
import com.real.musiclist.Dialog.UpdateSongDialog;
import com.real.musiclist.Model.Song;

import java.text.ParseException;
import java.text.RuleBasedCollator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import static com.real.musiclist.DiskActivity.admin;


public class SongActivity extends AppCompatActivity implements AdapterView.OnItemLongClickListener, AdapterView.OnItemClickListener, TextWatcher {

    ListView listOfSongs;
    ArrayAdapter arrayAdapter;
    ArrayList<Song> songs;
    DataBase dataBase;
    String diskID;
    String diskName;
    EditText editTextSort;
    ArrayList<Song> songsAfterFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song);
        dataBase = new DataBase(getApplicationContext());
        diskID = getIntent().getExtras().getString("diskID");
        diskName = getIntent().getExtras().getString("diskName");
        songs = dataBase.getAllSongList(diskID);
        sortListOfSinger();

        editTextSort = (EditText) findViewById(R.id.editTextSort);
        listOfSongs = (ListView) findViewById(R.id.listOfSongs);
        arrayAdapter = new SongAdapter(this, R.layout.item_of_song, songs);
        listOfSongs.setAdapter(arrayAdapter);
        listOfSongs.setOnItemLongClickListener(this);
        listOfSongs.setOnItemClickListener(this);

        editTextSort.addTextChangedListener(this);

            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabSong);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CreateSongDialog c = new CreateSongDialog();
                    c.show(getSupportFragmentManager(), "dialogOfSong");
                }
            });
        if(!admin) {
            fab.setVisibility(View.GONE);
        }

        setTitle("Диск №" + diskID + " " + diskName);

        songsAfterFilter = new ArrayList<>();
        songsAfterFilter.addAll(songs);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
               onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void filter (String charText){
            charText = charText.toLowerCase(Locale.getDefault());
            songs.clear();
            if (charText.length() == 0) {
                songs.addAll(songsAfterFilter);
            } else {
                for (Song wp : songsAfterFilter) {
                    if (wp.getSinger().toLowerCase(Locale.getDefault()).contains(charText)) {
                        songs.add(wp);
                    }
                }
                if (songs.isEmpty()) {
                    for (Song wp : songsAfterFilter) {
                        if (wp.getSongName().toLowerCase(Locale.getDefault()).contains(charText)) {
                            songs.add(wp);
                        }
                    }
                }
            }
            arrayAdapter.notifyDataSetChanged();
    }


    public void addSong(int id, String singer, String songName){
        if(!dataBase.getAllSongID(diskID).contains(id)) {
            dataBase.insertSong(id, singer, songName, Integer.parseInt(diskID));
            songs.add(new Song(id, singer, songName));
            songsAfterFilter.add(new Song(id, singer, songName));
//            Collections.sort(songs, new singerComparator());
            sortListOfSinger();
            arrayAdapter.notifyDataSetChanged();
        }else{
            Toast.makeText(this, "Песня с таким ID уже существует", Toast.LENGTH_LONG).show();
        }
    }

    public void deleteSong(String diskNumber, String id,int position){
        dataBase.deleteSong(diskNumber, id);
        songs.remove(position);
        songsAfterFilter.remove(position);
        arrayAdapter.notifyDataSetChanged();
    }

    public void updateSong(String id, String singer, String songName, String oldID, String diskID, int position){
        if(!dataBase.getAllSongID(diskID).contains(Integer.parseInt(id))) {
            dataBase.updateSong(id, singer, songName, oldID, diskID);
            songs.get(position).setId(Integer.parseInt(id));
            songs.get(position).setSinger(singer);
            songs.get(position).setSongName(songName);
            songsAfterFilter.get(position).setId(Integer.parseInt(id));
            songsAfterFilter.get(position).setSinger(singer);
            songsAfterFilter.get(position).setSongName(songName);
            arrayAdapter.notifyDataSetChanged();
        }else{
            Toast.makeText(this, "Песня с таким ID уже существует", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        if(admin) {
            DeleteSongDialog deleteSongDialog = new DeleteSongDialog();
            TextView tvID = (TextView) view.findViewById(R.id.tvID);
            TextView tvSongName = (TextView) view.findViewById(R.id.tvSong);
            String ID = tvID.getText().toString();
            String songName = tvSongName.getText().toString();
            Bundle bundle = new Bundle();
            bundle.putString("id", ID);
            bundle.putString("songName", songName);
            bundle.putInt("position", i);
            bundle.putString("diskID", diskID);
            deleteSongDialog.setArguments(bundle);
            deleteSongDialog.show(getSupportFragmentManager(), "deleteSongDialog");
        }
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if(admin) {
            UpdateSongDialog updateSongDialog = new UpdateSongDialog();
            TextView tvID = (TextView) view.findViewById(R.id.tvID);
            TextView tvSongName = (TextView) view.findViewById(R.id.tvSong);
            TextView tvSinger = (TextView) view.findViewById(R.id.tvSinger);
            String oldID = tvID.getText().toString();
            String songName = tvSongName.getText().toString();
            String singer = tvSinger.getText().toString();
            Bundle bundle = new Bundle();
            bundle.putString("diskID", diskID);
            bundle.putString("oldID", oldID);
            bundle.putString("songName", songName);
            bundle.putString("singer", singer);
            bundle.putInt("position", i);
            updateSongDialog.setArguments(bundle);
            updateSongDialog.show(getSupportFragmentManager(), "updateSongDialog");
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        filter(editable.toString());
    }


//    class singerComparator implements Comparator<Song> {
//        @Override
//        public int compare(Song objA, Song objB) {
//            return objA.getSinger().compareTo(objB.getSinger());
//        }
//    }

    private void sortListOfSinger(){
        String rule = "&А,а<Б,б<В,в<Г,г<Д,д<Е,е<Ё,ё<Ж,ж<З,з<И,и<Й,й<К,к<Л,л<М,м<Н,н" +
                "<О,о<П,п<Р,р<С,с<Т,т<У,у<Ф,ф<Х,х<Ц,ц<Ч,ч<Ш,ш<Щ,щ<Ъ,ъЫ,Ы,Ь,ь<Э,э<Ю,ю<Я,я" +
                "<A,a<B,b<C,c<D,d<E,e<F,f<G,g<H,h<I,i<J,j<K,k<L,l<M,m<N,n<O,o<P,p<Q,q" +
                "<R,r<S,s<T,t<U,u<V,v<W,w<X,x<Y,y<Z,z<0<1<2<3<4<5<6<7<8<9" +
                "<'!'<'@'<'#'<'$'<'%'<'^'<'&'<'*'<'('<')'<'_'<'+'<'±'<'§'<'<'<'>'<'/'<'?'<'.'<','<'\"'<';'<':'<'\\'<'|'<'`'<'~'<'['<']'<'{'<'}'";

        try {
            final RuleBasedCollator ruleBasedCollator = new RuleBasedCollator(rule);
            Collections.sort(songs, new Comparator<Song>() {
                @Override
                public int compare(Song objA, Song objB) {
                    return ruleBasedCollator.compare(objA.getSinger(), objB.getSinger());
                }
            });
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
