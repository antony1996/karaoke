package com.real.musiclist.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.real.musiclist.Model.Song;
import com.real.musiclist.R;

import java.util.ArrayList;


public class SongAdapter extends ArrayAdapter<Song> {
    Context context;
    ArrayList<Song> songsArray;


    public SongAdapter(Context context, int resource, ArrayList<Song> songsArray) {
        super(context, resource, songsArray);
        this.context = context;
        this.songsArray = songsArray;
    }

    static class ViewHolder {
        TextView tvID;
        TextView tvSinger;
        TextView tvSong;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_of_song, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvID = (TextView) convertView.findViewById(R.id.tvID);
            viewHolder.tvSinger = (TextView) convertView.findViewById(R.id.tvSinger);
            viewHolder.tvSong = (TextView) convertView.findViewById(R.id.tvSong);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvID.setText(String.valueOf(songsArray.get(position).getId()));
        viewHolder.tvSinger.setText(songsArray.get(position).getSinger());
        viewHolder.tvSong.setText(songsArray.get(position).getSongName());
        return convertView;
    }
}
