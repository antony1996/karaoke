package com.real.musiclist.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.real.musiclist.Model.Disk;
import com.real.musiclist.R;

import java.util.ArrayList;


public class DisksAdapter extends ArrayAdapter <Disk> {

    Context context;
    ArrayList<Disk> disksArray;

    public DisksAdapter(Context context, int resource, ArrayList<Disk> disksArray) {
        super(context, resource, disksArray);
        this.context = context;
        this.disksArray = disksArray;
    }

        static class ViewHolder {
            TextView tvNumberOfDisks;
            TextView tvDiskName;
        }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_of_disk, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvNumberOfDisks = (TextView) convertView.findViewById(R.id.tvNumberOfDisks);
            viewHolder.tvDiskName = (TextView) convertView.findViewById(R.id.tvDiskName);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvNumberOfDisks.setText(String.valueOf(disksArray.get(position).getNubmer()));
        viewHolder.tvDiskName.setText(disksArray.get(position).getName());
        return convertView;
    }


}
