package com.real.musiclist.Model;


public class Song {

    private int id;
    private String singer;
    private String songName;

    public Song(int id, String singer, String songName) {
        this.id = id;
        this.singer = singer;
        this.songName = songName;
    }



    public int getId() {
        return id;
    }

    public String getSinger() {
        return singer;
    }

    public String getSongName() {
        return songName;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }
}
